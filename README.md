ChainCommandBundle
=======================
Symfony bundle that implements command chaining functionality.
Other Symfony bundles in the application may register their console commands to be members of a command chain.
When a user runs the main command in a chain, all other commands registered in this chain should be executed as well. 
Commands registered as chain members can no longer be executed on their own.

Installation
------------

### Step 1: Add repository in composer.json

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://mr_Verman@bitbucket.org/yapa89/chaincommandbundle.git"
    }
  ]
}
```

### Step 2: Run update
```bash
composer require yapa89/chaincommandbundle:^1.0.0
```

### Step 3: Enable the Bundle
```php
<?php
// config/bundles.php

return [
     // .... 
     Yapa89\ChainCommandBundle\ChainCommandBundle::class => ['all' => true],
];
```

### Step 4: Add tag to command in services.yml 
```yml
services:
  barbundle.barcommand:
    class: Yapa89\BarBundle\Command\BarCommand
    tags:
      - { name: 'chain', run_with: 'foo:hello' }
      - { name: console.command }
```

Tests
------------
```bash
./bin/phpunit
```

