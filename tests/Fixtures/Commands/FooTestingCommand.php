<?php

declare(strict_types=1);

namespace Yapa89\ChainCommandBundle\Tests\Fixtures\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FooTestingCommand extends Command
{
    public function configure()
    {
        $this->setName('foo:testing');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Foo testing!');

        return Command::SUCCESS;
    }
}
