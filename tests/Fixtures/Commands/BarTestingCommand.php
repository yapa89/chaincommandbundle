<?php

declare(strict_types=1);

namespace Yapa89\ChainCommandBundle\Tests\Fixtures\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BarTestingCommand extends Command
{
    public function configure()
    {
        $this->setName('bar:testing');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Bar testing!');

        return Command::SUCCESS;
    }
}
