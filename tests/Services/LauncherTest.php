<?php

/*
 * Test Launcher class.
 *
 * (c) Pavel Yankovets <yanko.en@gmail.com>
 * doc <https://symfony.com/doc/current/console.html#testing-commands>
 */

declare(strict_types=1);

namespace Yapa89\ChainCommandBundle\Tests\Services;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Yapa89\ChainCommandBundle\Tests\Fixtures\Commands\BarTestingCommand;
use Yapa89\ChainCommandBundle\Tests\Fixtures\Commands\FooTestingCommand;

class LauncherTest extends KernelTestCase
{
    public function testChain()
    {
        $kernel = static::bootKernel();

        $barCommand = new BarTestingCommand();
        $fooCommand = new FooTestingCommand();

        $app = new Application($kernel);
        $app->setAutoExit(false);

        $collection = $this->getContainer()->get('chain_command_bundle.collection');
        $collection->add($barCommand, $fooCommand->getName());

        $app->add($barCommand);
        $app->add($fooCommand);

        $output = new BufferedOutput();
        $app->run(new ArrayInput([$fooCommand->getName()]), $output);

        $response = $output->fetch();

        $this->assertEquals('Foo testing!'.PHP_EOL.'Bar testing!'.PHP_EOL, $response);

        $output = new BufferedOutput();
        $app->run(new ArrayInput([$barCommand->getName()]), $output);

        $response = $output->fetch();

        $this->assertEquals(
            'Error: bar:testing command is a member of foo:testing'.
            ' command chain and cannot be executed on its own.'.PHP_EOL, $response);

        $output = new BufferedOutput();
        $app->run(new ArrayInput(['other']), $output);

        $response = $output->fetch();

        $this->assertEquals('Command "other" is not defined.', trim($response));
    }
}
