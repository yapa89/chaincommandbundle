<?php

/*
 * Test collection commands for chain.
 *
 * (c) Pavel Yankovets <yanko.en@gmail.com>
 */

declare(strict_types=1);

namespace Yapa89\ChainCommandBundle\Tests\Services;

use PHPUnit\Framework\TestCase;
use Yapa89\ChainCommandBundle\Services\Collection;
use Yapa89\ChainCommandBundle\Tests\Fixtures\Commands\BarTestingCommand;
use Yapa89\ChainCommandBundle\Tests\Fixtures\Commands\FooTestingCommand;

class CollectionTest extends TestCase
{
    public function testChildren()
    {
        $collection = new Collection();

        $barCommand = new BarTestingCommand();
        $fooCommand = new FooTestingCommand();

        $collection->add($barCommand, 'parent');
        $collection->add($fooCommand, 'parent');

        $children = $collection->getChildren('parent');

        $this->assertEquals([$barCommand, $fooCommand], $children);
    }

    public function testIsParent(): void
    {
        $collection = new Collection();
        $collection->add(new FooTestingCommand(), 'parent');

        $this->assertTrue($collection->isParent('parent'));
        $this->assertTrue($collection->isParent('other'));
        $this->assertFalse($collection->isParent('foo:testing'));
    }

    public function testGetChildren(): void
    {
        $collection = new Collection();
        $command = new BarTestingCommand();
        $collection->add($command, 'parent');

        $this->assertEquals($command, $collection->getChildren('parent')[0]);
    }

    public function testParentName()
    {
        $collection = new Collection();
        $collection->add(new FooTestingCommand(), 'parent');

        $this->assertEquals('parent', $collection->getParentName('foo:testing'));
        $this->assertEmpty($collection->getParentName('other'));
    }
}
