<?php

/*
 * Bundle for console chain.
 *
 * (c) Pavel Yankovets <yanko.en@gmail.com>
 */

declare(strict_types=1);

namespace Yapa89\ChainCommandBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Yapa89\ChainCommandBundle\DependencyInjection\CommandChainCompilerPass;

class ChainCommandBundle extends Bundle
{

    /**
     * Build
     *
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container): void
    {
        $container->addCompilerPass(new CommandChainCompilerPass());
    }

}
