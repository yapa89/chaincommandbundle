<?php

/*
 * Collection commands for chain.
 *
 * (c) Pavel Yankovets <yanko.en@gmail.com>
 */

declare(strict_types=1);

namespace Yapa89\ChainCommandBundle\Services;

use Symfony\Component\Console\Command\Command;

class Collection
{
    /**
     * Commands.
     */
    private array $commands = [];

    /**
     * Add commands.
     *
     * @param Command $command Command
     * @param string  $parent  Parent name
     */
    public function add(Command $command, string $parent): void
    {
        $this->commands[$command->getName()] = [
            'obj' => $command,
            'parent' => $parent,
        ];
    }

    /**
     * Check parent.
     *
     * @param string $commandName Command name
     */
    public function isParent(string $commandName): bool
    {
        return false === isset($this->commands[$commandName]);
    }

    /**
     * Get children by command name.
     *
     * @param string $commandName Command name
     */
    public function getChildren(string $commandName): array
    {
        return array_column(
            array_filter(
                $this->commands, function ($command) use ($commandName) {
                    return ($command['parent'] ?? '') === $commandName;
                }
            ), 'obj'
        );
    }

    /**
     * Get parent name by command name.
     *
     * @param string $commandName Command name
     */
    public function getParentName(string $commandName): string
    {
        return $this->commands[$commandName]['parent'] ?? '';
    }
}
