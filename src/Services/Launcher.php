<?php

/*
 * Class for launch console commands.
 *
 * (c) Pavel Yankovets <yanko.en@gmail.com>
 */

declare(strict_types=1);

namespace Yapa89\ChainCommandBundle\Services;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;

final class Launcher
{
    /**
     * Command for launch.
     */
    private Command $command;

    /**
     * Input command.
     */
    private InputInterface $input;

    /**
     * Output command.
     */
    private OutputInterface $output;

    /**
     * Launcher constructor.
     *
     * @param LoggerInterface $logger     Logger
     * @param Collection      $collection Commands collection
     */
    public function __construct(private LoggerInterface $logger, private Collection $collection)
    {
    }

    /**
     * Launch command.
     *
     * @param Command         $command Command for launch
     * @param InputInterface  $input   Input
     * @param OutputInterface $output  Output
     */
    public function launch(Command $command, InputInterface $input, OutputInterface $output): void
    {
        $this->command = $command;
        $this->input = $input;
        $this->output = $output;

        $this->beforeAllExecute();
        $this->allExecute();
    }

    /**
     * Actions before execute command.
     */
    private function beforeAllExecute(): void
    {
        if (false === empty($this->collection->getChildren($this->command->getName()))) {
            $this->logger->info(
                sprintf(
                    '%s is a master command of a command chain that has registered member commands',
                    $this->command->getName()
                )
            );

            $this->registerChain();
        }
    }

    /**
     * Execute all commands (include chain commands).
     */
    private function allExecute(): void
    {
        if (false === $this->collection->isParent($this->command->getName())) {
            $this->output->writeln(
                sprintf(
                    'Error: %s command is a member of %s command chain and cannot be executed on its own.',
                    $this->command->getName(),
                    $this->collection->getParentName($this->command->getName())
                )
            );

            return;
        }

        $this->parentExecute();
        $this->chainExecute();
    }

    /**
     * Register chain command.
     */
    private function registerChain(): void
    {
        array_map(
            function (Command $childCommand) {
                $this->logger->info(
                    sprintf(
                        '%s registered as a member of %s command chain',
                        $childCommand->getName(),
                        $this->collection->getParentName($childCommand->getName())
                    )
                );
            }, $this->collection->getChildren($this->command->getName())
        );
    }

    /**
     * Launch main command.
     */
    private function parentExecute(): void
    {
        $this->logger->info(
            sprintf(
                'Executing %s command itself first:',
                $this->command->getName()
            )
        );

        $output = $this->execute($this->command, $this->input, $this->output);
        $this->logger->info($output->fetch());
    }

    /**
     * Execute chain commands.
     */
    private function chainExecute(): void
    {
        $this->logger->info(
            sprintf(
                'Executing %s chain members:',
                $this->command->getName()
            )
        );

        array_map(
            function (Command $childCommand) {
                $buffedOutput = $this->execute($childCommand, new ArgvInput([]), $this->output);
                $this->logger->info($buffedOutput->fetch());
            }, $this->collection->getChildren($this->command->getName())
        );

        $this->logger->info(
            sprintf(
                'Execution of %s chain completed.',
                $this->command->getName()
            )
        );
    }

    /**
     * Execute command.
     *
     * @param Command         $command Command
     * @param InputInterface  $input   Input
     * @param OutputInterface $output  Output
     *
     * @throws \Exception
     */
    private function execute(Command $command, InputInterface $input, OutputInterface $output): BufferedOutput
    {
        $buffer = new BufferedOutput();
        $command->run($input, $buffer);

        $outputBuffer = $buffer->fetch();
        $buffer->write($outputBuffer);
        $output->write($outputBuffer);

        return $buffer;
    }
}
