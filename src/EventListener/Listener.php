<?php

/*
 * ClassListener console events.
 *
 * (c) Pavel Yankovets <yanko.en@gmail.com>
 */

declare(strict_types=1);

namespace Yapa89\ChainCommandBundle\EventListener;

use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Yapa89\ChainCommandBundle\Services\Launcher;

final class Listener implements EventSubscriberInterface
{
    /**
     * Listener constructor.
     *
     * @param Launcher $launcher
     */
    public function __construct(private Launcher $launcher)
    {
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents(): array
    {
        return [
            ConsoleEvents::COMMAND => 'console',
        ];
    }

    /** Event run command.
     *
     * @param ConsoleCommandEvent $event Event
     */
    public function console(ConsoleCommandEvent $event): void
    {
        if (false === $event->isPropagationStopped()) {
            $event->disableCommand();
            $this->launcher->launch($event->getCommand(), $event->getInput(), $event->getOutput());
        }
    }
}
