<?php

/*
 * Class CommandChainCompilerPass.
 *
 * (c) Pavel Yankovets <yanko.en@gmail.com>
 *
 * doc https://symfony.com/doc/current/service_container/tags.html
 */

declare(strict_types=1);

namespace Yapa89\ChainCommandBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class CommandChainCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        if (false === $container->has('chain_command_bundle.collection')) {
            return;
        }

        $definition = $container->findDefinition('chain_command_bundle.collection');
        $services = $container->findTaggedServiceIds('chain');

        foreach ($services as $id => $tags) {
            array_map(function ($tag) use ($id, $definition) {
                $definition->addMethodCall('add', [new Reference($id), $tag['run_with'] ?? '']);
            }, $tags);
        }
    }
}
