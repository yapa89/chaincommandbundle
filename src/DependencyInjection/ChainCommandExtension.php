<?php

/*
 * Class ChainCommandExtension.
 *
 * (c) Pavel Yankovets <yanko.en@gmail.com>
 */

declare(strict_types=1);

namespace Yapa89\ChainCommandBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class ChainCommandExtension extends Extension
{
    /**
     * Load Extension.
     *
     * @param array            $configs   Configs
     * @param ContainerBuilder $container Builder
     *
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }
}
